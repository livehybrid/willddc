<?php

/*
	Assumptions:
		Should be able to provide any file on the system (or remote?)



*/
if (PHP_SAPI != "cli") {
	print "This script is intended to be run from the command line";
	exit;
}

function __autoload($className){
	$includePath = "lib";
	$incFileName = "class." . $className . '.php';
	if(file_exists(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $includePath . DIRECTORY_SEPARATOR . $incFileName)){
		include_once(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $includePath . DIRECTORY_SEPARATOR . $incFileName);
		return;
	}
}


include "defaults.php";


//Check that the imput provides the required fields needed
$mySearcher = new searcher($defaultOptions);

$result = $mySearcher->runSearch();
print $result . PHP_EOL;

?>