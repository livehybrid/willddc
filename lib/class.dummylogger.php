<?php

class dummylogger extends logger {
	private $outputLevel;
	
	function __construct($logLevel) {
		$this->setLogLevel($logLevel);
	}
	function setLogLevel($logLevel) {
		switch ($logLevel) {
			case "LOG_EMERG" : $logLevel = 0; break;
			case "LOG_ALERT" : $logLevel = 1; break;
			case "LOG_CRIT" : $logLevel = 2; break;
			case "LOG_ERR" : $logLevel = 3; break;
			case "LOG_WARNING" : $logLevel = 4; break;
			case "LOG_NOTICE" : $logLevel = 5; break;
			case "LOG_INFO" : $logLevel = 6; break;
			case "LOG_DEBUG" : $logLevel = 7; break;
			default : $logLevel = 7; break;
		}
		$this->outputLevel = $logLevel;
	}
	
	function logData($splunkMsg,$msgLevel=LOG_NOTICE) {
		//Do nothing because this is a dummy logger	
		if ($msgLevel==$this->outputLevel) print $splunkMsg . PHP_EOL;
		return true;

	}
}

?>
