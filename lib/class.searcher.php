<?php 
	
	class searcher {
	private $defaultOptions = array();
	private $options = array();
	private $logger; 
	
	function __construct($defaultOptions,$localOptions=array()) {
		$this->defaultOptions = $defaultOptions;
		$this->options = $this->argHelper($localOptions);
		
		$logLevel = $this->options['logLevel'];
		
		//Define which logger we need
		if ($this->options['logging']===true) {
			$this->logger = new logger($logLevel,$this->options['logDirectory'],$this->options['logFilename']);
		} else {
			$this->logger = new dummylogger($logLevel);
		}
		//var_dump($this->options);
		$this->logger->logData("options".json_encode($this->options),LOG_DEBUG);
		set_error_handler(array($this->logger,"fatalError")); //Use a custom error handler to enable better logging
	}
	
	function argHelper($localOptions) {
		if (count($localOptions)==0) {
			$arguments = $GLOBALS['argv']; //Usually $argv is only available in the root context
			$helpText = 'USAGE: php search.php "Search Terms" SearchType [logLevel=LOG_DEBUG] [logging=true] [searchFile="texts/hscic-news"]' . PHP_EOL;
			$helpText .= 'SearchType must be "AND" or "OR"' . PHP_EOL;
	
			if ($arguments[1]==""||$arguments[1]=="help") die($helpText);
			
			$localOptions = array();
	
			$localOptions['terms'] = $arguments[1];
			$localOptions['searchType'] = $arguments[2];
			//Allow key-value pairs of optional parameters AFTER the first two required parameters. Optional parameters can be any order.
			for($i=2; $i<count($arguments); $i++) {
				$e = explode("=",$arguments[$i]);
				if(count($e)==2)
					$localOptions[$e[0]] = $e[1];
				else
					$localOptions[$e[0]] = null;
			}
		}
		
		//Merge the optional and default values
		$derivedOptions = array();
		$errorOutput = "";
		foreach ($this->defaultOptions as $optionKey => $option) {
			$derivedOptions[$optionKey] = (isset($localOptions[$optionKey])) ? $localOptions[$optionKey] : $option['val'];
			if ($option['required']&&$derivedOptions[$optionKey]=="") {
				//Required field is blank
				$errorOutput .= $option['errorMsg'] . PHP_EOL;
			}
		}

		if ($errorOutput!="") die($errorOutput . $helpText);
		return $derivedOptions;
	}

	function runSearch() {
		$terms = explode(" ",$this->options['terms']);

		$results = array();
		$fileData = file($this->options['searchFile']);
		
		foreach ($fileData as $pos => $line) {
			//print "$pos - " . $line . "\n";
			$results[$pos] = 0; //Define the counter
			foreach ($terms as $term) {
				$this->logger->logData("Searching file={$this->options['searchFile']} line=$pos for term=$term",LOG_DEBUG);
				if (stripos($line,$term)!==FALSE) {
					//This term exists in the line
					$results[$pos]++;
					$this->logger->logData("FOUND term=$term on line=$pos file={$this->options['searchFile']} ",LOG_DEBUG);
				}
			}
		}

		$found = array();
		$this->logger->logData("Performing a searchType={$this->options['searchType']} search on file={$this->options['searchFile']}",LOG_NOTICE);

		$maxAnswer = count($terms);

		foreach ($results as $lineNum => $result) {
			switch ($this->options['searchType']) {
				case "AND": if ($result==$maxAnswer) $found[] = $lineNum; break; //If AND then it must match the number of terms
				case "OR": if ($result>=1) $found[] = $lineNum; break; //If OR then only one term has to match
				default: die("Unexpected SearchType. Only AND or OR are accepted values.");
			}

		}

		$res = implode(",",$found);
		$foundCount = count($found);
		$this->logger->logData("Finished searchType={$this->options['searchType']} search on file={$this->options['searchFile']} - Found $foundCount matches",LOG_NOTICE);
		return $res;
	}

}
 
?>
