<?php

class logger {
	//Stripped down version of LiveHybrid_Splunkify (William Searle) for HSCIC Tech Test
	private $executionID = "";
	private $outputLevel = LOG_NOTICE;
	private $logDirectory = "";
	private $logFilename = "";

	function __construct($logLevel,$logDirectory,$logFilename) {
		$this->executionID = $this->makeguid();
		$this->setLogLevel($logLevel);
		$this->logDirectory = $logDirectory;
		$this->logFilename = $logFilename;
		date_default_timezone_set('UTC');
	}
	
	function setLogLevel($logLevel) {
		switch ($logLevel) {
			case "LOG_EMERG" : $logLevel = 0; break;
			case "LOG_ALERT" : $logLevel = 1; break;
			case "LOG_CRIT" : $logLevel = 2; break;
			case "LOG_ERR" : $logLevel = 3; break;
			case "LOG_WARNING" : $logLevel = 4; break;
			case "LOG_NOTICE" : $logLevel = 5; break;
			case "LOG_INFO" : $logLevel = 6; break;
			case "LOG_DEBUG" : $logLevel = 7; break;
			default : $logLevel = 7; break;
		}
		$this->outputLevel = $logLevel;
	}


	function makeguid() {
		return uniqid();
	}


	function logData($splunkMsg,$msgLevel=E_NOTICE) {
		//Higher the more verbose
		$callers=debug_backtrace();
		$d = new DateTime();
		
		$time = date("d/m/Y H:i:s") . substr((string)microtime(), 1, 8); //Microtime used to give accurate log reporting. Can be easily used to measure duration.
		//Record as key=value pairs for easy consumption by systems such as Splunk
		if ($msgLevel<=$this->outputLevel) {
			$logMessage = "$time - LogLevel=$msgLevel Message from class={$callers[1]['class']} function={$callers[1]['function']} - $splunkMsg - execID=".$this->executionID . PHP_EOL;
	
			$putComplete = @file_put_contents($this->logDirectory . DIRECTORY_SEPARATOR. $this->logFilename, $logMessage, FILE_APPEND);
			print $splunkMsg . PHP_EOL;
			if ($putComplete) return true;
		} else {
			return true;
		}

	}

	function fatalError($errNo, $errStr, $errFile, $errLine)
	{
		$errorMsg = "errorNo=$errNo error=\"$errStr\" errLine=$errLine in file=$errFile";
		if ($this->logData($errorMsg,LOG_CRIT)) {
			//print PHP_EOL . $errorMsg . PHP_EOL;
			throw new Exception($errorMsg);
			die();
			//die($errorMsg);
		} else {
			return false; //Fall back on PHP's Internal Error Handler
		}
	}

}

?>
