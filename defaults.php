<?php
$APPDIR = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
$defaultOptions = array();
$defaultOptions['terms'] = array("val"=>"","required"=>1,"errorMsg"=>"Missing required input: Search Terms (First parameter)"); //This cannot be blank
$defaultOptions['searchType'] = array("val"=>"","required"=>1,"errorMsg"=>"Missing required input: Search Type (AND/OR) (Second parameter)"); //This cannot be blank
$defaultOptions['logging'] = array("val"=>true,"required"=>0);
$defaultOptions['searchFile'] = array("val"=>$APPDIR . "texts/news","required"=>1);
$defaultOptions['logFilename'] = array("val"=>"splunk.log","required"=>1);
//$defaultOptions['searchFile'] = array("val"=>"https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw/ef09a74e925e051c6a756ec54b6adba1787d937b/hscic-news","required"=>0);
$defaultOptions['logDirectory'] = array("val"=>$APPDIR . "logs","required"=>1);
$defaultOptions['logLevel'] = array("val"=>"LOG_CRIT","required"=>1);


?>
