<?php
	
$APPDIR = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR;
define('APPDIR',$APPDIR);

class StackTest extends PHPUnit_Framework_TestCase
{
	private $defaultOptions;
	
	function setUp() {
		
		include APPDIR."defaults.php";
		$this->defaultOptions = $defaultOptions;
	}
	
    public function testCase1()
    {
	    
		
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'OR';
		$localOptions['terms'] = 'Care Quality Commission';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/news';
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
		$result = $mySearcher->runSearch();
		fwrite(STDOUT, PHP_EOL . "Searching for terms=\"{$localOptions['terms']}\" with searchType={$localOptions['searchType']} in file={$localOptions['searchFile']} RESULT=$result" . PHP_EOL);
		$this->assertEquals('0,1,2,3,4,5,6', $result);
		
    }
    
     public function testCase2()
    {
	    
		
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'OR';
		$localOptions['terms'] = 'September 2004';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/news';
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
		$result = $mySearcher->runSearch();
		fwrite(STDOUT, PHP_EOL . "Searching for terms=\"{$localOptions['terms']}\" with searchType={$localOptions['searchType']} in file={$localOptions['searchFile']} RESULT=$result" . PHP_EOL);
		$this->assertEquals('9', $result);
		
    }
    
     public function testCase3()
    {
	    
		
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'OR';
		$localOptions['terms'] = 'general population generally';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/news';
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
		$result = $mySearcher->runSearch();
		fwrite(STDOUT, PHP_EOL . "Searching for terms=\"{$localOptions['terms']}\" with searchType={$localOptions['searchType']} in file={$localOptions['searchFile']} RESULT=$result" . PHP_EOL);
		$this->assertEquals('6,8', $result);
		
    }
    
     public function testCase4()
    {
	    
		
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'AND';
		$localOptions['terms'] = 'Care Quality Commission admission';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/news';
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
		$result = $mySearcher->runSearch();
		fwrite(STDOUT, PHP_EOL . "Searching for terms=\"{$localOptions['terms']}\" with searchType={$localOptions['searchType']} in file={$localOptions['searchFile']} RESULT=$result" . PHP_EOL);
		$this->assertEquals('1', $result);
		
    }
    
     public function testCase5()
    {
	    
		
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'AND';
		$localOptions['terms'] = 'general population Alzheimer';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/news';
		
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
		$result = $mySearcher->runSearch();
		
		fwrite(STDOUT, PHP_EOL . "Searching for terms=\"{$localOptions['terms']}\" with searchType={$localOptions['searchType']} in file={$localOptions['searchFile']} RESULT=$result" . PHP_EOL);
		
		$this->assertEquals('6', $result);
		
    }
    
    public function testErrorCatcher() {
	    
		//These would usually be argv values passed at runtime
		$localOptions = array();
		$localOptions['searchType'] = 'AND';
		$localOptions['terms'] = 'general population Alzheimer';

		$localOptions['logLevel'] = "LOG_CRIT";
		$localOptions['logging'] = false;
		$localOptions['searchFile'] = APPDIR.'texts/NON-EXISTANT-FILE';
		
	    $mySearcher = new searcher($this->defaultOptions,$localOptions);
	    fwrite(STDOUT, PHP_EOL . "Testing for thrown exception when file is not found" . PHP_EOL);
	    try {
		$result = $mySearcher->runSearch();
    } catch (Exception $e) {
		
		$this->assertInstanceOf('Exception',$e);
   
    }
   } 
    
}
?>
