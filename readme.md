## Search
###Overview:
Searcher is a PHP CLI script to perform AND/OR searches on data file, returning the index of each line within the data file that matches the criteria. 

####Directory structure & files:
* lib/ - Directory containing class files used by searcher.php
* logs/ - Directory for keeping log files generated
* phpunit/ - Directory containing PHPUnit tests
* texts/ - Contains files to be searched
* defaults.php - Contains default configuration options
* search.php - Main application file to run the search
* sanityTest.sh - Multiple calls to search.php as examples, and for quick CLI testing


### Usage
```
Usage: php search.php TERMS SEARCHTYPE [OptionalKey=Val]
```
| OptionalKey        | Default Value           | Notes  |
| ------------- |:-------------:| -----:|
| searchFile      | ./texts/news | The default file is a local copy of https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  |
| logDirectory      | ./logs      |   Set the logging directory, the user must have read-write rights to this folder.  |
| logging | true      |    This enables logging to 'splunk.log' (or overwritten 'logFilename' value) within the logDirectory. Set to  false to disable. |
| logFilename | splunk.log      |    Alter the filename of the logging output. |
| logLevel | LOG_NOTICE      |    Set this anything between LOG_CRIT (0) to LOG_DEBUG (7) |
Note: Default values are relative to the application. Overwritten values should be relative to the working directory, or absolute. 


### Example usage

```
php search.php "Care Quality Commission" OR searchFile="https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw"
```

##### STDOUT
```
0,1,2,3,4,5,6
```

##### logs/splunk.log
```
28/01/2016 22:03:52.2434600 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=0 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2438410 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=1 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2456190 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Quality on line=1 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2456880 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Commission on line=1 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2457820 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=2 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2458980 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=3 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2459770 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=4 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2461050 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=5 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2461940 - LogLevel=7 Message from class=searcher function=runSearch - FOUND term=Care on line=6 file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw  - execID=56aa9047715ef
28/01/2016 22:03:52.2463330 - LogLevel=6 Message from class=searcher function=runSearch - Performing a searchType=OR search on file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw - execID=56aa9047715ef
28/01/2016 22:03:52.2463890 - LogLevel=6 Message from class=searcher function=runSearch - Finished searchType=OR search on file=https://gist.github.com/edhiley/fdf7793d3d2c9e838c11/raw - Found 7 matches - execID=56aa9047715ef
```



## Dependencies
* PHP >= 5.5.30 [Tested version]

###Installing

#### Windows
See: [http://php.net/manual/en/install.windows.php](http://php.net/manual/en/install.windows.php)

#### Linux

* apt-get install php5



## Testing

### "SanityTest"
The sanity test file (sanityTest.sh) can be found in the root of the app directory and executes the searcher app through the CLI to produce the expected outcomes as per the criteria. This fully tests the basics of the application.


### PHPUnit Testing
From the application directory, run:

```
phpunit --bootstrap phpunit/autoload.php phpunit/testing.php
```
(Requires PHPUnit to be installed)

####Output

```
HPUnit 5.1.4 by Sebastian Bergmann and contributors.


Searching for terms="Care Quality Commission" with searchType=OR in file=/Users/williamsearle/DDCTest/phpunit/../texts/news RESULT=0,1,2,3,4,5,6
.
Searching for terms="September 2004" with searchType=OR in file=/Users/williamsearle/DDCTest/phpunit/../texts/news RESULT=9
.
Searching for terms="general population generally" with searchType=OR in file=/Users/williamsearle/DDCTest/phpunit/../texts/news RESULT=6,8
.
Searching for terms="Care Quality Commission admission" with searchType=AND in file=/Users/williamsearle/DDCTest/phpunit/../texts/news RESULT=1
.
Searching for terms="general population Alzheimer" with searchType=AND in file=/Users/williamsearle/DDCTest/phpunit/../texts/news RESULT=6
.
Testing for thrown exception when file is not found
.                                                              6 / 6 (100%)

Time: 87 ms, Memory: 10.25Mb

OK (6 tests, 6 assertions)

```

### Docker
During the process, Docker has been used for testing on Ubuntu 14.04 (Latest).
The docker image is available at [https://hub.docker.com/u/livehybrid/ddc](https://hub.docker.com/u/livehybrid/ddc)
####Output
```
➜  ~  docker run livehybrid/ddc
0,1,2,3,4,5,6
9
6,8
1
6

```

